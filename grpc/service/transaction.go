package service

import (
	"checkout_service/config"
	"checkout_service/genproto/checkout_service"
	"checkout_service/grpc/client"
	"checkout_service/packages/logger"
	"checkout_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type TransactionService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*checkout_service.UnimplementedTransactionServiceServer
}

func NewTransactionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *TransactionService {
	return &TransactionService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *TransactionService) Create(ctx context.Context, req *checkout_service.CreateTransaction) (resp *checkout_service.Transaction, err error) {

	i.log.Info("---CreateTransaction------>", logger.Any("req", req))

	pKey, err := i.strg.Transaction().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateTransaction->Transaction->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Transaction().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyTransaction->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *TransactionService) GetByID(ctx context.Context, req *checkout_service.TransactionPrimaryKey) (resp *checkout_service.Transaction, err error) {

	i.log.Info("---GetTransactionByID------>", logger.Any("req", req))

	resp, err = i.strg.Transaction().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetTransactionByID->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *TransactionService) GetList(ctx context.Context, req *checkout_service.GetListTransactionRequest) (resp *checkout_service.GetListTransactionResponse, err error) {

	i.log.Info("---GetTransactions------>", logger.Any("req", req))

	resp, err = i.strg.Transaction().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetTransactions->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *TransactionService) Update(ctx context.Context, req *checkout_service.UpdateTransaction) (resp *checkout_service.Transaction, err error) {

	i.log.Info("---UpdateTransaction------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Transaction().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateTransaction--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Transaction().GetByPKey(ctx, &checkout_service.TransactionPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetTransaction->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *TransactionService) Delete(ctx context.Context, req *checkout_service.TransactionPrimaryKey) (resp *checkout_service.Empty, err error) {

	i.log.Info("---DeleteTransaction------>", logger.Any("req", req))

	err = i.strg.Transaction().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteTransaction->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &checkout_service.Empty{}, nil
}
