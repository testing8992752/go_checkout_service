package service

import (
	"checkout_service/config"
	"checkout_service/genproto/checkout_service"
	"checkout_service/grpc/client"
	"checkout_service/packages/logger"
	"checkout_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Payment struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*checkout_service.UnimplementedPaymentServiceServer
}

func NewPaymentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *Payment {
	return &Payment{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *Payment) Create(ctx context.Context, req *checkout_service.CreatePayment) (resp *checkout_service.Payment, err error) {

	i.log.Info("---CreatePayment------>", logger.Any("req", req))

	pKey, err := i.strg.Payment().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreatePayment->Payment->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Payment().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyPayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *Payment) GetByID(ctx context.Context, req *checkout_service.PaymentPrimaryKey) (resp *checkout_service.Payment, err error) {

	i.log.Info("---GetPayment------>", logger.Any("req", req))

	resp, err = i.strg.Payment().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetPayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *Payment) GetList(ctx context.Context, req *checkout_service.GetListPaymentRequest) (resp *checkout_service.GetListPaymentResponse, err error) {

	i.log.Info("---GetPayment------>", logger.Any("req", req))

	resp, err = i.strg.Payment().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetPayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *Payment) Update(ctx context.Context, req *checkout_service.UpdatePayment) (resp *checkout_service.Payment, err error) {

	i.log.Info("---UpdatePayment------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Payment().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdatePayment--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Payment().GetByPKey(ctx, &checkout_service.PaymentPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetPayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *Payment) Delete(ctx context.Context, req *checkout_service.PaymentPrimaryKey) (resp *checkout_service.Empty, err error) {

	i.log.Info("---DeletePayment------>", logger.Any("req", req))

	err = i.strg.Payment().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeletePayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &checkout_service.Empty{}, nil
}
