package service

import (
	"checkout_service/config"
	"checkout_service/genproto/checkout_service"
	"checkout_service/grpc/client"
	"checkout_service/packages/logger"
	"checkout_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SaleService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*checkout_service.UnimplementedSaleServiceServer
}

func NewSaleService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SaleService {
	return &SaleService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *SaleService) Create(ctx context.Context, req *checkout_service.CreateSale) (resp *checkout_service.Sale, err error) {
	i.log.Info("---CreateSale------>", logger.Any("req", req))

	shift, err := i.strg.Shift().GetByPKey(ctx, &checkout_service.ShiftPrimaryKey{Id: req.ShiftId})
	if err != nil {
		i.log.Error("Error while getting shist Id", logger.Error(err))
	}
	if shift.Status == "Open" {
		pKey, err := i.strg.Sale().Create(ctx, req)
		if err != nil {
			i.log.Error("!!!CreateSale->Sale->Create--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		resp, err = i.strg.Sale().GetByPKey(ctx, pKey)
		if err != nil {
			i.log.Error("!!!GetByPKeySale->Sale->Get--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	} else {
		i.log.Error("у вас нет открытых касс, сначала откройте кассу")
	}

	return
}

func (i *SaleService) GetByID(ctx context.Context, req *checkout_service.SalePrimaryKey) (resp *checkout_service.Sale, err error) {

	i.log.Info("---GetSaleByID------>", logger.Any("req", req))

	resp, err = i.strg.Sale().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSaleByID->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleService) GetList(ctx context.Context, req *checkout_service.GetListSaleRequest) (resp *checkout_service.GetListSaleResponse, err error) {

	i.log.Info("---GetSales------>", logger.Any("req", req))

	resp, err = i.strg.Sale().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSales->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleService) Update(ctx context.Context, req *checkout_service.UpdateSale) (resp *checkout_service.Sale, err error) {

	i.log.Info("---UpdateSale------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Sale().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSale--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Sale().GetByPKey(ctx, &checkout_service.SalePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSale->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SaleService) Delete(ctx context.Context, req *checkout_service.SalePrimaryKey) (resp *checkout_service.Empty, err error) {

	i.log.Info("---DeleteSale------>", logger.Any("req", req))

	err = i.strg.Sale().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSale->Sale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &checkout_service.Empty{}, nil
}
