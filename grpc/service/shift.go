package service

import (
	"checkout_service/config"
	"checkout_service/genproto/checkout_service"
	"checkout_service/grpc/client"
	"checkout_service/packages/logger"
	"checkout_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ShiftService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*checkout_service.UnimplementedShiftServiceServer
}

func NewShiftService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ShiftService {
	return &ShiftService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (u *ShiftService) Create(ctx context.Context, req *checkout_service.CreateShift) (resp *checkout_service.Shift, err error) {
	u.log.Info("----------Shift Create------->", logger.Any("req", req))

	shifts, err := u.strg.Shift().GetAll(ctx, &checkout_service.GetListShiftRequest{})
	if err != nil {
		u.log.Error("Error While GetList Shift", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	for _, val := range shifts.Shifts {
		if val.BranchId == req.BranchId && val.Status == "Open" {
			u.log.Error("касса уже открыта. Сначала закройте", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, "касса уже открыта. Сначала закройте")
		}
	}

	pKey, err := u.strg.Shift().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Shift", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = u.strg.Shift().GetByPKey(ctx, pKey)
	if err != nil {
		u.log.Error("Error While GetByID ShiftID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}


func (i *ShiftService) GetByID(ctx context.Context, req *checkout_service.ShiftPrimaryKey) (resp *checkout_service.Shift, err error) {

	i.log.Info("---GetShiftByID------>", logger.Any("req", req))

	resp, err = i.strg.Shift().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShiftByID->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShiftService) GetList(ctx context.Context, req *checkout_service.GetListShiftRequest) (resp *checkout_service.GetListShiftResponse, err error) {

	i.log.Info("---GetShifts------>", logger.Any("req", req))

	resp, err = i.strg.Shift().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShifts->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShiftService) Update(ctx context.Context, req *checkout_service.UpdateShift) (resp *checkout_service.Shift, err error) {

	i.log.Info("---UpdateShift------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Shift().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateShift--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Shift().GetByPKey(ctx, &checkout_service.ShiftPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetShift->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ShiftService) Delete(ctx context.Context, req *checkout_service.ShiftPrimaryKey) (resp *checkout_service.Empty, err error) {

	i.log.Info("---DeleteShift------>", logger.Any("req", req))

	err = i.strg.Shift().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteShift->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &checkout_service.Empty{}, nil
}
