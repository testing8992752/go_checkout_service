package service

import (
	"checkout_service/config"
	"checkout_service/genproto/checkout_service"
	"checkout_service/grpc/client"
	"checkout_service/packages/logger"
	"checkout_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SaleProductsService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*checkout_service.UnimplementedSaleProductServiceServer
}

func NewSaleProductsService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SaleProductsService {
	return &SaleProductsService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *SaleProductsService) Create(ctx context.Context, req *checkout_service.CreateSaleProduct) (resp *checkout_service.SaleProduct, err error) {

	i.log.Info("---CreateSaleProduct------>", logger.Any("req", req))

	pKey, err := i.strg.SaleProduct().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateSaleProduct->SaleProduct->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.SaleProduct().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeySaleProduct->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleProductsService) GetByID(ctx context.Context, req *checkout_service.SaleProductPrimaryKey) (resp *checkout_service.SaleProduct, err error) {

	i.log.Info("---GetSaleProductByID------>", logger.Any("req", req))

	resp, err = i.strg.SaleProduct().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSaleProductByID->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleProductsService) GetList(ctx context.Context, req *checkout_service.GetListSaleProductRequest) (resp *checkout_service.GetListSaleProductResponse, err error) {

	i.log.Info("---GetSaleProducts------>", logger.Any("req", req))

	resp, err = i.strg.SaleProduct().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetSaleProducts->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *SaleProductsService) Update(ctx context.Context, req *checkout_service.UpdateSaleProduct) (resp *checkout_service.SaleProduct, err error) {

	i.log.Info("---UpdateSaleProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.SaleProduct().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateSaleProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.SaleProduct().GetByPKey(ctx, &checkout_service.SaleProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetSaleProduct->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *SaleProductsService) Delete(ctx context.Context, req *checkout_service.SaleProductPrimaryKey) (resp *checkout_service.Empty, err error) {

	i.log.Info("---DeleteSaleProduct------>", logger.Any("req", req))

	err = i.strg.SaleProduct().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteSaleProduct->SaleProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &checkout_service.Empty{}, nil
}
