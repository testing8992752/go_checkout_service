package grpc

import (
	"checkout_service/config"
	"checkout_service/genproto/checkout_service"
	"checkout_service/grpc/client"
	"checkout_service/grpc/service"
	"checkout_service/packages/logger"
	"checkout_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	checkout_service.RegisterSaleServiceServer(grpcServer, service.NewSaleService(cfg, log, strg, srvc))
	checkout_service.RegisterSaleProductServiceServer(grpcServer, service.NewSaleProductsService(cfg, log, strg, srvc))
	checkout_service.RegisterPaymentServiceServer(grpcServer, service.NewPaymentService(cfg, log, strg, srvc))
	checkout_service.RegisterShiftServiceServer(grpcServer, service.NewShiftService(cfg, log, strg, srvc))
	checkout_service.RegisterTransactionServiceServer(grpcServer, service.NewTransactionService(cfg, log, strg, srvc))
	reflection.Register(grpcServer)
	return
}
