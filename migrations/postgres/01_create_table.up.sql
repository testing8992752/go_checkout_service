-- shift(смена)
CREATE TABLE "shift" (
  "id" UUID PRIMARY KEY,
  "increment_id" VARCHAR,
  "branch_id" UUID,
  "user_id" UUID,
  "sale_point" UUID,
  "status" VARCHAR(20),
  "open_shift"  VARCHAR,
  "close_shift" VARCHAR, 
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);



-- transaction
CREATE TABLE "transaction" (
  "id" UUID PRIMARY KEY,
  "shift_id" UUID NOT NULL REFERENCES "shift"("id"),
  "cash"  NUMERIC,
  "uzcard"  NUMERIC,
  "payme"  NUMERIC,
  "click"  NUMERIC,
  "humo"  NUMERIC,
  "apelsin"  NUMERIC,
  "total_amount" NUMERIC,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);


-- sale
CREATE TABLE "sale" (
  "id" UUID PRIMARY KEY,
  "increment_id" VARCHAR(255),
  "branch_id" UUID,
  "sale_point_id" UUID,
  "shift_id" UUID NOT NULL REFERENCES "shift"("id"),
  "employee_id" UUID,
  "barcode" VARCHAR(500),
  "status" VARCHAR(20), 
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);


-- sale_products
CREATE TABLE "sale_products" (
  "id" UUID PRIMARY KEY,
  "sale_id" UUID NOT NULL REFERENCES "sale"("id"),
  "category_id" UUID,
  "product_id" UUID,
  "barcode" VARCHAR(50),
  "remaining_quantity" INT,
  "quantity" INT,
  "allow_discount" BOOLEAN,
  "discount_type" VARCHAR(20),
  "discount_price" NUMERIC,
  "price" NUMERIC,
  "total_amount" NUMERIC, 
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);


CREATE TABLE "payment" (
  "id" UUID PRIMARY KEY,
  "sale_id"  UUID NOT NULL REFERENCES "sale"("id"),
  "cash" NUMERIC,
  "uzcard" NUMERIC,
  "payme" NUMERIC,
  "click" NUMERIC,
  "humo" NUMERIC,
  "apelsin" NUMERIC,
  "total_amount" NUMERIC, 
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP
);