package storage

import (
	"checkout_service/genproto/checkout_service"
	"context"
)

type StorageI interface {
	CloseDB()
	SaleProduct() SaleProductRepoI
	Sale() SaleRepoI
	Payment() PaymentRepoI
	Transaction() TransactionRepoI
	Shift() ShiftRepoI
}

type PaymentRepoI interface {
	Create(ctx context.Context, req *checkout_service.CreatePayment) (resp *checkout_service.PaymentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *checkout_service.PaymentPrimaryKey) (resp *checkout_service.Payment, err error)
	GetAll(ctx context.Context, req *checkout_service.GetListPaymentRequest) (resp *checkout_service.GetListPaymentResponse, err error)
	Update(ctx context.Context, req *checkout_service.UpdatePayment) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *checkout_service.PaymentPrimaryKey) error
}

type SaleRepoI interface {
	Create(ctx context.Context, req *checkout_service.CreateSale) (resp *checkout_service.SalePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *checkout_service.SalePrimaryKey) (resp *checkout_service.Sale, err error)
	GetAll(ctx context.Context, req *checkout_service.GetListSaleRequest) (resp *checkout_service.GetListSaleResponse, err error)
	Update(ctx context.Context, req *checkout_service.UpdateSale) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *checkout_service.SalePrimaryKey) error
}

type SaleProductRepoI interface {
	Create(ctx context.Context, req *checkout_service.CreateSaleProduct) (resp *checkout_service.SaleProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *checkout_service.SaleProductPrimaryKey) (resp *checkout_service.SaleProduct, err error)
	GetAll(ctx context.Context, req *checkout_service.GetListSaleProductRequest) (resp *checkout_service.GetListSaleProductResponse, err error)
	Update(ctx context.Context, req *checkout_service.UpdateSaleProduct) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *checkout_service.SaleProductPrimaryKey) error
}

type TransactionRepoI interface {
	Create(ctx context.Context, req *checkout_service.CreateTransaction) (resp *checkout_service.TransactionPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *checkout_service.TransactionPrimaryKey) (resp *checkout_service.Transaction, err error)
	GetAll(ctx context.Context, req *checkout_service.GetListTransactionRequest) (resp *checkout_service.GetListTransactionResponse, err error)
	Update(ctx context.Context, req *checkout_service.UpdateTransaction) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *checkout_service.TransactionPrimaryKey) error
}

type ShiftRepoI interface {
	Create(ctx context.Context, req *checkout_service.CreateShift) (resp *checkout_service.ShiftPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *checkout_service.ShiftPrimaryKey) (resp *checkout_service.Shift, err error)
	GetAll(ctx context.Context, req *checkout_service.GetListShiftRequest) (resp *checkout_service.GetListShiftResponse, err error)
	Update(ctx context.Context, req *checkout_service.UpdateShift) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *checkout_service.ShiftPrimaryKey) error
}



