package memory

import (
	"checkout_service/genproto/checkout_service"
	"checkout_service/packages/helper"
	"checkout_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type SaleProductRepo struct {
	db *pgxpool.Pool
}

func NewSaleProductRepo(db *pgxpool.Pool) storage.SaleProductRepoI {
	return &SaleProductRepo{
		db: db,
	}
}

func (c *SaleProductRepo) Create(ctx context.Context, req *checkout_service.CreateSaleProduct) (resp *checkout_service.SaleProductPrimaryKey, err error) {

	var id = uuid.New().String()

	query := `
		INSERT INTO "sale_products" (
			id,
			sale_id,
			category_id,
			product_id,
			barcode,
			remaining_quantity,
			quantity,
			allow_discount,
			discount_type,
			discount_price,
			price,
			total_amount,
			updated_at) VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.SaleId,
		req.CategoryId,
		req.ProductId,
		req.Barcode,
		req.RemainingQuantity,
		req.Quantity,
		req.AllowDiscount,
		req.DiscountType,
		req.Price/10*float32(req.Quantity),
		req.Price,
		req.Price*float32(req.Quantity)-(req.Price*float32(req.Quantity)/10),
	)

	if err != nil {
		return nil, err
	}

	return &checkout_service.SaleProductPrimaryKey{Id: id}, nil
}

func (c *SaleProductRepo) GetByPKey(ctx context.Context, req *checkout_service.SaleProductPrimaryKey) (resp *checkout_service.SaleProduct, err error) {

	query := `
		SELECT
			id,
			sale_id,
			category_id,
			product_id,
			barcode,
			remaining_quantity,
			quantity,
			allow_discount,
			discount_type,
			discount_price,
			price,
			total_amount,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sale_products"
		WHERE id = $1
	`

	var (
		id                sql.NullString
		saleId            sql.NullString
		categoryId        sql.NullString
		productId         sql.NullString
		barcode           sql.NullString
		remainingQuantity sql.NullInt64
		quantity          sql.NullInt64
		allowDiscount     sql.NullBool
		discountType      sql.NullString
		discountPrice     sql.NullFloat64
		price             sql.NullFloat64
		totalAmount       sql.NullFloat64
		createdAt         sql.NullString
		updatedAt         sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&saleId,
		&categoryId,
		&productId,
		&barcode,
		&remainingQuantity,
		&quantity,
		&allowDiscount,
		&discountType,
		&discountPrice,
		&price,
		&totalAmount,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &checkout_service.SaleProduct{
		Id:                id.String,
		SaleId:            saleId.String,
		CategoryId:        categoryId.String,
		ProductId:         productId.String,
		Barcode:           barcode.String,
		RemainingQuantity: remainingQuantity.Int64,
		Quantity:          quantity.Int64,
		AllowDiscount:     allowDiscount.Bool,
		DiscountType:      discountType.String,
		DiscountPrice:     float32(discountPrice.Float64),
		Price:             float32(price.Float64),
		TotalAmount:       float32(totalAmount.Float64),
		CreatedAt:         createdAt.String,
		UpdatedAt:         updatedAt.String,
	}

	return
}

func (c *SaleProductRepo) GetAll(ctx context.Context, req *checkout_service.GetListSaleProductRequest) (resp *checkout_service.GetListSaleProductResponse, err error) {

	resp = &checkout_service.GetListSaleProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			sale_id,
			category_id,
			product_id,
			barcode,
			remaining_quantity,
			quantity,
			allow_discount,
			discount_type,
			discount_price,
			price,
			total_amount,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sale_products"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id                sql.NullString
			saleId            sql.NullString
			categoryId        sql.NullString
			productId         sql.NullString
			barcode           sql.NullString
			remainingQuantity sql.NullInt64
			quantity          sql.NullInt64
			allowDiscount     sql.NullBool
			discountType      sql.NullString
			discountPrice     sql.NullFloat64
			price             sql.NullFloat64
			totalAmount       sql.NullFloat64
			createdAt         sql.NullString
			updatedAt         sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&saleId,
			&categoryId,
			&productId,
			&barcode,
			&remainingQuantity,
			&quantity,
			&allowDiscount,
			&discountType,
			&discountPrice,
			&price,
			&totalAmount,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.SaleProducts = append(resp.SaleProducts, &checkout_service.SaleProduct{
			Id:                id.String,
			SaleId:            saleId.String,
			CategoryId:        categoryId.String,
			ProductId:         productId.String,
			Barcode:           barcode.String,
			RemainingQuantity: remainingQuantity.Int64,
			Quantity:          quantity.Int64,
			AllowDiscount:     allowDiscount.Bool,
			DiscountType:      discountType.String,
			DiscountPrice:     float32(discountPrice.Float64),
			Price:             float32(price.Float64),
			TotalAmount:       float32(totalAmount.Float64),
			CreatedAt:         createdAt.String,
			UpdatedAt:         updatedAt.String,
		})
	}

	return
}

func (c *SaleProductRepo) Update(ctx context.Context, req *checkout_service.UpdateSaleProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "sale_products"
			SET
				id = :id,
				sale_id = :sale_id,
				category_id = :category_id,
				product_id = :product_id,
				barcode = :barcode,
				remaining_quantity = :remaining_quantity,
				quantity = :quantity,
				allow_discount = :allow_discount,
				discount_type = :discount_type,
				discount_price = :discount_price,
				price = :price,
				total_amount = :total_amount,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":                 req.GetId(),
		"sale_id":            req.GetSaleId(),
		"category_id":        req.GetCategoryId(),
		"product_id":         req.GetProductId(),
		"barcode":            req.GetBarcode(),
		"remaining_quantity": req.GetRemainingQuantity(),
		"quantity":           req.GetQuantity(),
		"allow_discount":     req.GetAllowDiscount(),
		"discount_type":      req.GetDiscountType(),
		"discount_price":     req.GetDiscountPrice(),
		"price":              req.GetPrice(),
		"total_amount":       req.GetPrice()*float32(req.GetQuantity()) - (req.GetPrice() * float32(req.GetQuantity()) / 10),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *SaleProductRepo) Delete(ctx context.Context, req *checkout_service.SaleProductPrimaryKey) error {

	query := `DELETE FROM "sale_products" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
