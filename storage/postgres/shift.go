package memory

import (
	"checkout_service/genproto/checkout_service"
	"checkout_service/packages/helper"
	"checkout_service/storage"
	"context"
	"database/sql"
	"strconv"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ShiftRepo struct {
	db *pgxpool.Pool
}

func NewShiftRepo(db *pgxpool.Pool) storage.ShiftRepoI {
	return &ShiftRepo{
		db: db,
	}
}

func (c *ShiftRepo) Create(ctx context.Context, req *checkout_service.CreateShift) (resp *checkout_service.ShiftPrimaryKey, err error) {

	var (
		id           = uuid.New().String()
		increment_id = ""
	)

	max_query := `
		SELECT MAX(increment_id) 
			FROM "shift"
		`
	err = c.db.QueryRow(ctx, max_query).Scan(&increment_id)
	if err != nil {
		if err.Error() != "can't scan into dest[0]: cannot scan null into *string" {
			return resp, err
		} else {
			increment_id = "C-0000000"
		}
	}

	digit, _ := strconv.Atoi(increment_id[2:])

	query := `
		INSERT INTO "shift" (
			id,
			increment_id,
			branch_id,
			user_id,
			sale_point,
			status,
			open_shift,
			close_shift,
			updated_at) VALUES
			($1, $2, $3, $4, $5, $6, TO_TIMESTAMP($7, 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP($8, 'YYYY-MM-DD HH24:MI:SS'), NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		"P-"+helper.GetSerialId(digit),
		req.BranchId,
		req.UserId,
		req.SalePoint,
		"Open",
		req.OpenShift,
		req.CloseShift,
	)

	if err != nil {
		return nil, err
	}

	return &checkout_service.ShiftPrimaryKey{Id: id}, nil
}

func (c *ShiftRepo) GetByPKey(ctx context.Context, req *checkout_service.ShiftPrimaryKey) (resp *checkout_service.Shift, err error) {

	query := `
		SELECT
			id,
			increment_id,
			branch_id,
			user_id,
			sale_point,
			status,
			open_shift,
			close_shift,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "shift"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		incrementId sql.NullString
		branchId    sql.NullString
		userId      sql.NullString
		salePoint   sql.NullString
		status      sql.NullString
		openShift   sql.NullString
		closeShift  sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&incrementId,
		&branchId,
		&userId,
		&salePoint,
		&status,
		&openShift,
		&closeShift,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &checkout_service.Shift{
		Id:          id.String,
		IncrementId: incrementId.String,
		BranchId:    branchId.String,
		UserId:      userId.String,
		SalePoint:   salePoint.String,
		Status:      status.String,
		OpenShift:   openShift.String,
		CloseShift:  closeShift.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *ShiftRepo) GetAll(ctx context.Context, req *checkout_service.GetListShiftRequest) (resp *checkout_service.GetListShiftResponse, err error) {

	resp = &checkout_service.GetListShiftResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			increment_id,
			branch_id,
			user_id,
			sale_point,
			status,
			open_shift,
			close_shift,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "shift"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			incrementId sql.NullString
			branchId    sql.NullString
			userId      sql.NullString
			salePoint   sql.NullString
			status      sql.NullString
			openShift   sql.NullString
			closeShift  sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&incrementId,
			&branchId,
			&userId,
			&salePoint,
			&status,
			&openShift,
			&closeShift,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Shifts = append(resp.Shifts, &checkout_service.Shift{
			Id:          id.String,
			IncrementId: incrementId.String,
			BranchId:    branchId.String,
			UserId:      userId.String,
			SalePoint:   salePoint.String,
			Status:      status.String,
			OpenShift:   openShift.String,
			CloseShift:  closeShift.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *ShiftRepo) Update(ctx context.Context, req *checkout_service.UpdateShift) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "shift"
			SET
				id = :id,
				branch_id = :branch_id,
				user_id = :user_id,
				sale_point = :sale_point,
				status = :status,
				open_shift = :open_shift,
				close_shift = :close_shift,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"branch_id":   req.GetBranchId(),
		"user_id":     req.GetUserId(),
		"sale_point":  req.GetSalePoint(),
		"status":      req.GetStatus(),
		"open_shift":  req.GetOpenShift(),
		"close_shift": req.GetCloseShift(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ShiftRepo) Delete(ctx context.Context, req *checkout_service.ShiftPrimaryKey) error {

	query := `DELETE FROM "shift" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
