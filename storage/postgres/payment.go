package memory

import (
	"checkout_service/genproto/checkout_service"
	"checkout_service/packages/helper"
	"checkout_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type PaymentRepo struct {
	db *pgxpool.Pool
}

func NewPaymentRepo(db *pgxpool.Pool) storage.PaymentRepoI {
	return &PaymentRepo{
		db: db,
	}
}

func (c *PaymentRepo) Create(ctx context.Context, req *checkout_service.CreatePayment) (resp *checkout_service.PaymentPrimaryKey, err error) {

	var id = uuid.New().String()

	query := `
		INSERT INTO "payment" (
			id,
			sale_id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			total_amount,
			updated_at) VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, $9, NOW())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.SaleId,
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Apelsin,
		req.Apelsin+req.Cash+req.Click+req.Humo+req.Payme+req.Uzcard,
	)

	if err != nil {
		return nil, err
	}

	return &checkout_service.PaymentPrimaryKey{Id: id}, nil
}

func (c *PaymentRepo) GetByPKey(ctx context.Context, req *checkout_service.PaymentPrimaryKey) (resp *checkout_service.Payment, err error) {

	query := `
		SELECT
			id,
			sale_id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			total_amount,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "payment"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		saleId      sql.NullString
		cash        sql.NullFloat64
		uzcard      sql.NullFloat64
		payme       sql.NullFloat64
		click       sql.NullFloat64
		humo        sql.NullFloat64
		apelsin     sql.NullFloat64
		totalAmount sql.NullFloat64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&saleId,
		&cash,
		&uzcard,
		&payme,
		&click,
		&humo,
		&apelsin,
		&totalAmount,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &checkout_service.Payment{
		Id:          id.String,
		SaleId:      saleId.String,
		Cash:        float32(cash.Float64),
		Uzcard:      float32(uzcard.Float64),
		Payme:       float32(payme.Float64),
		Click:       float32(click.Float64),
		Humo:        float32(humo.Float64),
		Apelsin:     float32(apelsin.Float64),
		TotalAmount: float32(totalAmount.Float64),
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *PaymentRepo) GetAll(ctx context.Context, req *checkout_service.GetListPaymentRequest) (resp *checkout_service.GetListPaymentResponse, err error) {

	resp = &checkout_service.GetListPaymentResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			sale_id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			total_amount,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "payment"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			saleId      sql.NullString
			cash        sql.NullFloat64
			uzcard      sql.NullFloat64
			payme       sql.NullFloat64
			click       sql.NullFloat64
			humo        sql.NullFloat64
			apelsin     sql.NullFloat64
			totalAmount sql.NullFloat64
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&saleId,
			&cash,
			&uzcard,
			&payme,
			&click,
			&humo,
			&apelsin,
			&totalAmount,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Payments = append(resp.Payments, &checkout_service.Payment{
			Id:          id.String,
			SaleId:      saleId.String,
			Cash:        float32(cash.Float64),
			Uzcard:      float32(uzcard.Float64),
			Payme:       float32(payme.Float64),
			Click:       float32(click.Float64),
			Humo:        float32(humo.Float64),
			Apelsin:     float32(apelsin.Float64),
			TotalAmount: float32(totalAmount.Float64),
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *PaymentRepo) Update(ctx context.Context, req *checkout_service.UpdatePayment) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "payment"
			SET
				id = :id,
				sale_id = :sale_id,
				cash = :cash,
				uzcard = :uzcard,
				payme = :payme,
				click = :click,
				humo = :humo,
				apelsin = :apelsin,
				total_amount = :total_amount,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"sale_id":      req.GetSaleId(),
		"cash":         req.GetCash(),
		"uzcard":       req.GetUzcard(),
		"payme":        req.GetPayme(),
		"click":        req.GetClick(),
		"humo":         req.GetHumo(),
		"apelsin":      req.GetApelsin(),
		"total_amount": req.GetCash() + req.GetUzcard() + req.GetPayme() + req.GetClick() + req.GetHumo() + req.GetApelsin(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *PaymentRepo) Delete(ctx context.Context, req *checkout_service.PaymentPrimaryKey) error {

	query := `DELETE FROM "payment" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
