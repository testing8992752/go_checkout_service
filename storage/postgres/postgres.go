package memory

import (
	"checkout_service/config"
	"checkout_service/storage"
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Store struct {
	db          *pgxpool.Pool
	sale        storage.SaleRepoI
	saleProduct storage.SaleProductRepoI
	payment     storage.PaymentRepoI
	transaction storage.TransactionRepoI
	shift       storage.ShiftRepoI
}

type Pool struct {
	db *pgxpool.Pool
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (l *Store) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	args := make([]interface{}, 0, len(data)+2) // making space for arguments + level + msg
	args = append(args, level, msg)
	for k, v := range data {
		args = append(args, fmt.Sprintf("%s=%v", k, v))
	}
	log.Println(args...)
}

func (s *Store) Shift() storage.ShiftRepoI {
	if s.shift == nil {
		s.shift = NewShiftRepo(s.db)
	}

	return s.shift
}

func (s *Store) SaleProduct() storage.SaleProductRepoI {
	if s.saleProduct == nil {
		s.saleProduct = NewSaleProductRepo(s.db)
	}

	return s.saleProduct
}

func (s *Store) Sale() storage.SaleRepoI {
	if s.sale == nil {
		s.sale = NewSaleRepo(s.db)
	}

	return s.sale
}


func (s *Store) Payment() storage.PaymentRepoI {
	if s.payment == nil {
		s.payment = NewPaymentRepo(s.db)
	}

	return s.payment
}


func (s *Store) Transaction() storage.TransactionRepoI {
	if s.transaction == nil {
		s.transaction = NewTransactionRepo(s.db)
	}

	return s.transaction
}