package memory

import (
	"checkout_service/genproto/checkout_service"
	"checkout_service/packages/helper"
	"checkout_service/storage"
	"context"
	"database/sql"
	"strconv"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type SaleRepo struct {
	db *pgxpool.Pool
}

func NewSaleRepo(db *pgxpool.Pool) storage.SaleRepoI {
	return &SaleRepo{
		db: db,
	}
}

func (c *SaleRepo) Create(ctx context.Context, req *checkout_service.CreateSale) (resp *checkout_service.SalePrimaryKey, err error) {
	var (
		id           = uuid.New().String()
		increment_id = ""
	)

	maxQuery := `
	SELECT MAX(increment_id) 
	FROM "sale"
	`
	err = c.db.QueryRow(ctx, maxQuery).Scan(&increment_id)
	if err != nil {
		if err.Error() != "can't scan into dest[0]: cannot scan null into *string" && err.Error() != "no rows in result set" {
			return resp, err
		} else {
			increment_id = "S-0000000"
		}
	}

	digit := 0
	if len(increment_id) > 2 {
		digit, err = strconv.Atoi(increment_id[2:])
		if err != nil {
			return resp, err
		}
	}

	query := `
	INSERT INTO "sale" (
		id,
		increment_id,
		branch_id,
		sale_point_id,
		shift_id,
		employee_id,
		barcode,
		status,
		updated_at) VALUES
		($1, $2, $3, $4, $5, $6, $7, $8, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		"S-"+helper.GetSerialId(digit),
		req.BranchId,
		req.SalePointId,
		req.ShiftId,
		req.EmployeeId,
		req.Barcode,
		"new",
	)

	if err != nil {
		return nil, err
	}

	return &checkout_service.SalePrimaryKey{Id: id}, nil
}

func (c *SaleRepo) GetByPKey(ctx context.Context, req *checkout_service.SalePrimaryKey) (resp *checkout_service.Sale, err error) {

	query := `
		SELECT
			id,
			increment_id,
			branch_id,
			sale_point_id,
			shift_id,
			employee_id,
			barcode,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sale"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		incrementId sql.NullString
		branchId    sql.NullString
		salePointId sql.NullString
		shiftId     sql.NullString
		employeeId  sql.NullString
		barcode     sql.NullString
		status      sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&incrementId,
		&branchId,
		&salePointId,
		&shiftId,
		&employeeId,
		&barcode,
		&status,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &checkout_service.Sale{
		Id:          id.String,
		IncrementId: incrementId.String,
		BranchId:    branchId.String,
		SalePointId: salePointId.String,
		ShiftId:     shiftId.String,
		EmployeeId:  employeeId.String,
		Barcode:     barcode.String,
		Status:      status.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *SaleRepo) GetAll(ctx context.Context, req *checkout_service.GetListSaleRequest) (resp *checkout_service.GetListSaleResponse, err error) {

	resp = &checkout_service.GetListSaleResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			increment_id,
			branch_id,
			sale_point_id,
			shift_id,
			employee_id,
			barcode,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sale"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			incrementId sql.NullString
			branchId    sql.NullString
			salePointId sql.NullString
			shiftId     sql.NullString
			employeeId  sql.NullString
			barcode     sql.NullString
			status      sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&incrementId,
			&branchId,
			&salePointId,
			&shiftId,
			&employeeId,
			&barcode,
			&status,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Sales = append(resp.Sales, &checkout_service.Sale{
			Id:          id.String,
			IncrementId: incrementId.String,
			BranchId:    branchId.String,
			SalePointId: salePointId.String,
			ShiftId:     shiftId.String,
			EmployeeId:  employeeId.String,
			Barcode:     barcode.String,
			Status:      status.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *SaleRepo) Update(ctx context.Context, req *checkout_service.UpdateSale) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "sale"
			SET
				id = :id,
				increment_id = :increment_id,
				branch_id = :branch_id,
				sale_point_id = :sale_point_id,
				shift_id = :shift_id,
				employee_id = :employee_id,
				barcode = :barcode,
				status = :status,
				updated_at = NOW()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":            req.GetId(),
		"increment_id":  req.GetIncrementId(),
		"branch_id":     req.GetBranchId(),
		"sale_point_id": req.GetSalePointId(),
		"shift_id":      req.GetShiftId(),
		"employee_id":   req.GetEmployeeId(),
		"barcode":       req.GetBarcode(),
		"status":        req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *SaleRepo) Delete(ctx context.Context, req *checkout_service.SalePrimaryKey) error {

	query := `DELETE FROM "sale" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
